import React from "react";
import PersistentDrawerRight from "./screens/DrawerNavigation";
class App extends React.Component {
  constructor() {
    super();
    this.state = { isLoggedIn: false };
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogin() {
    this.setState({ isLoggedIn: true });
  }
  handleLogout() {
    this.setState({ isLoggedIn: false });
  }
  render() {
    return (
      <div className="bg-light" style={{ height: "100%" }}>
        <PersistentDrawerRight
          handleLogin={this.handleLogin}
          handleLogout={this.handleLogout}
        />
      </div>
    );
  }
}

export default App;
