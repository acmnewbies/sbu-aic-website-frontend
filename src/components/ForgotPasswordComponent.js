import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { isMobile } from "react-device-detect";
class ForgotPasswordModal extends React.Component {
  render() {
    return (
      <div
        className="p-2 border border-info rounded"
        style={{ borderWidth: "10px" }}
      >
        <label className="m-2" htmlFor="email">
          ایمیل
        </label>
        <div className="d-flex flex-row">
          <input
            name="email"
            type="text"
            className="form-control m-2"
            width="2px"
          />
          <button type="submit" className="btn btn-primary m-2">
            send forgot email
          </button>
        </div>

        <div className="form-group d-flex justify-content-center"></div>
      </div>
    );
  }
}

export default ForgotPasswordModal;
