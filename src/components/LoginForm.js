import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Popup from "reactjs-popup";
import ForgotPasswordModal from "../components/ForgotPasswordComponent";
import CircularProgress from "@material-ui/core/CircularProgress";
class LoginForm extends React.Component {
  constructor() {
    super();
    this.state = { showPassword: false };
    this.eyeIcon = this.eyeIcon.bind(this);
  }

  eyeIcon(props) {
    if (this.state.showPassword) {
      return (
        <VisibilityIcon className={props.className} onClick={props.onClick} />
      );
    }
    return (
      <VisibilityOffIcon className={props.className} onClick={props.onClick} />
    );
  }
  getValidationClass(touched, error) {
    if (touched && error) {
      return " is-invalid";
    } else if (touched && !error) {
      return " is-valid";
    } else return "";
  }

  render() {
    return (
      <Formik
        initialValues={{
          email: "",
          password: ""
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string().required("لطفا ایمیل خود را وارد کنید"),
          password: Yup.string().required("لطفا رمز عبور خود را وارد کنید")
        })}
        onSubmit={fields => {
          this.props.onSubmit(fields);
        }}
        render={({ errors, status, touched }) => (
          <Form style={{ flex: 0.7 }} dir="rtl">
            <div className="form-group">
              <label htmlFor="email">ایمیل</label>
              <Field
                name="email"
                type="text"
                className={
                  "form-control" +
                  this.getValidationClass(touched.email, errors.email)
                }
              />
              <ErrorMessage
                name="email"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <div className="d-flex flex-row align-items-center  justify-content-between ">
                <label htmlFor="password">رمز عبور</label>

                <this.eyeIcon
                  className="m-2"
                  onClick={() => {
                    if (this.state.showPassword)
                      this.setState({ showPassword: false });
                    else this.setState({ showPassword: true });
                  }}
                ></this.eyeIcon>
              </div>
              <Field
                name="password"
                type={this.state.showPassword ? "text" : "password"}
                className={
                  "form-control" +
                  this.getValidationClass(touched.password, errors.password)
                }
              />
              <ErrorMessage
                name="password"
                component="div"
                className="invalid-feedback"
              />
            </div>

            <div
              className="form-group d-flex justify-content-center align-items-center"
              style={{ width: "1" }}
            >
              {!this.props.isPosting && (
                <div>
                  <button type="submit" className="btn btn-primary mr-2">
                    ورود
                  </button>
                  <Popup
                    className="modal"
                    trigger={
                      <button type="button" className="btn btn-link">
                        رمز عبور خود را فراموش کرده‌اید؟
                      </button>
                    }
                    modal
                    closeOnDocumentClick
                  >
                    <ForgotPasswordModal></ForgotPasswordModal>
                  </Popup>
                </div>
              )}
              {this.props.isPosting && <CircularProgress />}
            </div>
          </Form>
        )}
      />
    );
  }
}

export default LoginForm;
