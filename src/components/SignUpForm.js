import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import CircularProgress from "@material-ui/core/CircularProgress";
class SignUpForm extends React.Component {
  constructor() {
    super();
    this.state = { showPassword: false };
    this.eyeIcon = this.eyeIcon.bind(this);
  }

  eyeIcon(props) {
    if (this.state.showPassword) {
      return (
        <VisibilityIcon className={props.className} onClick={props.onClick} />
      );
    }
    return (
      <VisibilityOffIcon className={props.className} onClick={props.onClick} />
    );
  }
  getValidationClass(touched, error) {
    if (touched && error) {
      return " is-invalid";
    } else if (touched && !error) {
      return " is-valid";
    } else return "";
  }

  render() {
    return (
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          confirmPassword: ""
        }}
        validationSchema={Yup.object().shape({
          firstName: Yup.string().required("لطفا نام خود را وارد کنید"),
          lastName: Yup.string().required("لطفا نام خانوادگی خود را وارد کنید"),
          email: Yup.string()
            .email("ایمیل وارد شده نادرست است")
            .required("لطفا ایمیل خود را وارد کنید"),
          password: Yup.string()
            .min(6, "رمز عبور باید بیش از ۶ کاراکتر باشد")
            .required("لطفا رمز عبور خود را وارد کنید"),
          confirmPassword: Yup.string()
            .oneOf([Yup.ref("password"), null], "با رمز عبور یکسان نیست")
            .required("لطفا رمز عبور خود را وارد کنید")
        })}
        onSubmit={fields => {
          this.props.onSubmit(fields);
        }}
        render={({ errors, status, touched }) => (
          <Form style={{ flex: 0.7 }} dir="rtl">
            <div className="form-group">
              <label>نام</label>
              <Field
                name="firstName"
                type="text"
                className={
                  "form-control" +
                  this.getValidationClass(touched.firstName, errors.firstName)
                }
              />
              <ErrorMessage
                name="firstName"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <label htmlFor="lastName">نام خانوادگی</label>
              <Field
                name="lastName"
                type="text"
                className={
                  "form-control" +
                  this.getValidationClass(touched.lastName, errors.lastName)
                }
              />
              <ErrorMessage
                name="lastName"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <label htmlFor="email">ایمیل</label>
              <Field
                dir="ltr"
                name="email"
                type="text"
                className={
                  "form-control" +
                  this.getValidationClass(touched.email, errors.email)
                }
              />
              <ErrorMessage
                name="email"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group">
              <div className="d-flex flex-row align-items-center  justify-content-between ">
                <label htmlFor="password">رمز عبور</label>

                <this.eyeIcon
                  className="m-2"
                  onClick={() => {
                    if (this.state.showPassword)
                      this.setState({ showPassword: false });
                    else this.setState({ showPassword: true });
                  }}
                ></this.eyeIcon>
              </div>
              <Field
                name="password"
                type={this.state.showPassword ? "text" : "password"}
                className={
                  "form-control" +
                  this.getValidationClass(touched.password, errors.password)
                }
              />
              <ErrorMessage
                name="password"
                component="div"
                className="invalid-feedback"
              />
            </div>

            <div className="form-group">
              <label htmlFor="confirmPassword">تأیید رمز عبور</label>
              <Field
                name="confirmPassword"
                type="password"
                className={
                  "form-control" +
                  this.getValidationClass(
                    touched.confirmPassword,
                    errors.confirmPassword
                  )
                }
              />
              <ErrorMessage
                name="confirmPassword"
                component="div"
                className="invalid-feedback"
              />
            </div>
            <div className="form-group d-flex justify-content-center">
              {!this.props.isPosting && (
                <div className="d-flex flex-fill justify-content-between">
                  <button type="submit" className="btn btn-primary mr-2">
                    ثبت نام
                  </button>
                  <button type="reset" className="btn btn-secondary ">
                    از نو
                  </button>
                </div>
              )}
              {this.props.isPosting && <CircularProgress />}
            </div>
          </Form>
        )}
      />
    );
  }
}

export default SignUpForm;
