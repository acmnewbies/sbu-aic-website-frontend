import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import SignUpForm from "../components/SignUpForm";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { isMobile } from "react-device-detect";
import axios from "axios";
import { BASE_URL } from "../Constants/NetWorkConsts";
import SweetAlert from "react-bootstrap-sweetalert";
import { withRouter } from "react-router-dom";

import Cookie from "js-cookie";
import CircularProgress from "@material-ui/core/CircularProgress";

class SignUp extends React.Component {
  constructor() {
    super();
    this.state = {
      postSuccess: false,
      isPosting: false,
      postFailed: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getCardStyle() {
    if (isMobile || window.innerWidth <= 700) return { flex: 1 };
    else return { flex: 0.5 };
  }

  handleSubmit = async data => {
    await this.setState({ isPosting: true });
    axios
      .post(BASE_URL + "/signup", data)
      .then(res => {
        this.setState({ postSuccess: true, isPosting: false });
        // alert("ثبت نام با موفّقیّت انجام شد.");
      })
      .catch(err => {
        this.setState({ postFailed: true, isPosting: false });
        console.log(err);
      });
  };

  render() {
    return (
      <div className="container-fluid d-flex pt-5 mt-5 justify-content-center bg-light">
        {/* <button
          onClick={() => {
            const token = Cookie.get("token");
            alert(token);
          }}
        ></button> */}
        <div className="card bg-light" style={this.getCardStyle()}>
          <div className="card-header bg-info"></div>
          <div className="card-body d-flex flex-row align-content-center justify-content-center">
            <SignUpForm
              isPosting={this.state.isPosting}
              onSubmit={this.handleSubmit}
            ></SignUpForm>
            {this.state.postSuccess && (
              <SweetAlert
                success
                title="ثبت نام با موفّقیّت انجام شد"
                confirmBtnBsStyle="success"
                confirmBtnText="ورود"
                onConfirm={() => {
                  this.setState({ postSuccess: false });
                  this.props.history.push({
                    pathname: "/login"
                  });
                }}
              >
                قبل از ورود ایمیل خود را تایید کنید
              </SweetAlert>
            )}
            {this.state.postFailed && (
              <SweetAlert
                danger
                title="مشکلی در ثبت نام شما رخ داد"
                confirmBtnBsStyle="danger"
                confirmBtnText="باشه"
                onConfirm={() => {
                  this.setState({ postFailed: false });
                }}
              >
                از اتصال اینترنت خود مطمئن شوید
              </SweetAlert>
            )}
          </div>
          <div className="card-footer bg-info"></div>
        </div>
      </div>
    );
  }
}

export default withRouter(SignUp);
