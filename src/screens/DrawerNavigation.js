import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Profile from "./ProfileScreen";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Avatar from "@material-ui/core/Avatar";
import AssignmentIndOutlinedIcon from "@material-ui/icons/AssignmentIndOutlined";

import SignUp from "./SignUpScreen";
import Login from "./LoginScreen";
import Home from "./MainScreen";
import { from } from "rxjs";
import Cookie from "js-cookie";

const drawerWidth = 150;

const requestConfig = {
  headers: { Authorization: "Bearer ".concat(Cookie.get("token")) }
};

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    backgroundColor: "#5bc0de"
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginRight: drawerWidth
  },
  title: {
    flexGrow: 1
  },
  hide: {
    display: "none"
  },

  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "black"
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginRight: 0
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginRight: 0
  }
}));

export default function PersistentDrawerRight(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const onLogOut = () => {
    Cookie.remove("token");
    props.handleLogout();
  };

  const getDrawerButtons = () => {
    if (Cookie.get("token") === "" || Cookie.get("token") == null) {
      return (
        <div>
          <Link to="/signUp">
            <button type="submit" className="btn btn-dark ml-4">
              ثبت نام
            </button>
          </Link>

          <Link to="/login">
            <button type="submit" className="btn btn-dark">
              ورود
            </button>
          </Link>
        </div>
      );
    } else {
      return (
        <div className="d-flex flex-row">
          <Link to="/profile" className="d-flex justify-content-end">
            <Avatar className="bg-dark">
              <AssignmentIndOutlinedIcon />
            </Avatar>
          </Link>
          <button type="reset" onClick={onLogOut} className="btn btn-dark mr-3">
            خروج
          </button>
        </div>
      );
    }
  };

  return (
    <Router>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open
        })}
      >
        <Toolbar className="d-flex justify-content-between align-items-center">
          {getDrawerButtons()}
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="end"
            onClick={handleDrawerOpen}
            className={clsx(open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      <div
        className="d-flex align-content-center justify-content-center"
        style={{ flex: 1 }}
      >
        <Switch>
          <Route path="/signUp">
            <SignUp></SignUp>
          </Route>
          <Route path="/login">
            <Login handleLogin={props.handleLogin}></Login>
          </Route>{" "}
          <Route path="/home">
            <Home></Home>
          </Route>
          <Route path="/profile">
            <Profile requestConfig={requestConfig}></Profile>
          </Route>
        </Switch>
      </div>
      <Drawer
        variant="persistent"
        anchor="right"
        open={open}
        classes={{
          paper: [classes.drawerPaper, "bg-light"].join(" ")
        }}
      >
        <div className="bg-light">
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List className="bg-light" dir="rtl">
          <Link to="/home">
            <ListItem button>
              <ListItemIcon>
                <HomeOutlinedIcon />
              </ListItemIcon>
              <span style={{ fontSize: "1em", fontWeight: "bold" }}>خانه</span>
            </ListItem>
          </Link>
          <Link to="/login" className="d-flex justify-content-end">
            <ListItem button dir="rtl">
              <ListItemIcon>
                <AssignmentIndOutlinedIcon />
              </ListItemIcon>
              <span
                style={{
                  fontSize: "1em",
                  fontWeight: "bold"
                }}
              >
                پروفال
              </span>
            </ListItem>
          </Link>
        </List>
        <Divider />
        <List></List>
      </Drawer>
    </Router>
  );
}
