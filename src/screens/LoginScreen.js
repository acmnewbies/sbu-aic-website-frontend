import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { isMobile } from "react-device-detect";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Popup from "reactjs-popup";
import ForgotPasswordModal from "../components/ForgotPasswordComponent";
import axios from "axios";
import { BASE_URL } from "../Constants/NetWorkConsts";
import LoginForm from "../components/LoginForm";
import { withRouter } from "react-router-dom";

import SweetAlert from "react-bootstrap-sweetalert";
import Cookie from "js-cookie";
class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      postSuccess: false,
      isPosting: false,
      postFailed: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  getCardStyle() {
    if (isMobile || window.innerWidth <= 700) return { flex: 1 };
    else return { flex: 0.5 };
  }

  handleSubmit = async data => {
    await this.setState({ isPosting: true });
    axios
      .post(BASE_URL + "/login", data)
      .then(res => {
        this.setState({ postSuccess: true, isPosting: false });
        // alert("ثبت نام با موفّقیّت انجام شد.");

        Cookie.set("token", res.data["token"]);
        this.props.handleLogin();
        console.log(JSON.stringify(res.data["token"]));
      })
      .catch(err => {
        this.setState({ postFailed: true, isPosting: false });
        console.log(err);
      });
  };

  render() {
    return (
      <div className="container-fluid d-flex pt-5 mt-5 justify-content-center bg-light">
        <div className="card bg-light" style={this.getCardStyle()}>
          <div className="card-header bg-info"></div>
          <div className="card-body d-flex flex-row align-content-center justify-content-center">
            <LoginForm
              isPosting={this.state.isPosting}
              onSubmit={this.handleSubmit}
            ></LoginForm>
            {this.state.postSuccess && (
              <SweetAlert
                success
                title="ورود با موفّقیّت انجام شد"
                confirmBtnBsStyle="success"
                confirmBtnText="اوکی"
                onConfirm={() => {
                  this.setState({ postSuccess: false });
                  this.props.history.push({
                    pathname: "/home"
                  });
                }}
              >
                !خوش آمدید
              </SweetAlert>
            )}
            {this.state.postFailed && (
              <SweetAlert
                danger
                title=""
                confirmBtnBsStyle="danger"
                confirmBtnText="باشه"
                onConfirm={() => {
                  this.setState({ postFailed: false });
                }}
              >
                ایمیل و یا رمز عبور وارد شده نادرست است
              </SweetAlert>
            )}
          </div>
          <div className="card-footer bg-info"></div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
