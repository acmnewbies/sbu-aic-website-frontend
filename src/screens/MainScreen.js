import React from "react";
import mainImage from "../images/mainPageImg.jpg";
import { withRouter } from "react-router-dom";

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      postSuccess: false,
      isPosting: false,
      postFailed: false
    };
  }

  render() {
    return (
      <div className="mt-5" style={{ width: "100%" }}>
        <img src={mainImage} style={{ width: "100%" }} alt="Responsive"></img>
      </div>
    );
  }
}

export default withRouter(Home);
