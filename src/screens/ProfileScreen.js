import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { isMobile } from "react-device-detect";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Popup from "reactjs-popup";
import ForgotPasswordModal from "../components/ForgotPasswordComponent";
import axios from "axios";
import { BASE_URL } from "../Constants/NetWorkConsts";
import LoginForm from "../components/LoginForm";
import { withRouter } from "react-router-dom";
import ProfileLayout from "../components/ProfilePageLayout";
import SweetAlert from "react-bootstrap-sweetalert";

import Cookie from "js-cookie";
class Profile extends React.Component {
  constructor() {
    super();
    this.state = {
      getSuccess: false,
      isGetting: false,
      getFailed: false,
      data: null
    };
  }
  getCardStyle() {
    if (isMobile || window.innerWidth <= 700) return { flex: 1 };
    else return { flex: 1 };
  }

  componentDidMount() {
    this.setState({ isGetting: true });
    console.log("ASDASDAS ===> " + JSON.stringify(this.props.requestConfig));
    axios
      .get(BASE_URL + "/profile", {
        headers: { Authorization: "Bearer " + Cookie.get("token") }
      })
      .then(res => {
        this.setState({ getSuccess: true, isGetting: false, data: res.data });
        alert(JSON.stringify(res.data));
      })
      .catch(err => {
        this.setState({ getFailed: true, isGetting: false });
        console.log("erreeeeeeeeeee : " + JSON.stringify(err));
      });
  }

  render() {
    return (
      <div className="container-fluid d-flex pt-5 mt-5 justify-content-center bg-light">
        <div className="card bg-light" style={this.getCardStyle()}>
          <div className="card-header bg-info"></div>
          <div className="card-body d-flex flex-row align-content-center justify-content-center">
            {this.state.getFailed && (
              <SweetAlert
                danger
                title=""
                confirmBtnBsStyle="danger"
                confirmBtnText="باشه"
                onConfirm={() => {
                  this.setState({ getFailed: false });
                  this.props.history.push({
                    pathname: "/home"
                  });
                }}
              >
                مشکلی رخ‌داده
              </SweetAlert>
            )}
            {this.state.getSuccess && (
              <ProfileLayout data={this.state.data}></ProfileLayout>
            )}
          </div>
          <div className="card-footer bg-info"></div>
        </div>
      </div>
    );
  }
}

export default withRouter(Profile);
